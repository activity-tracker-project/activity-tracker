<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"></script>
    <title>JavaScript Computations</title>
</head>
<body>
    <h2>1. Konversi Suhu</h2>
    <label for="tempInput">Masukkan suhu:</label>
    <input type="number" id="tempInput" placeholder="Masukkan suhu" />
    
    <label for="fromUnit">Dari satuan:</label>
    <select id="fromUnit">
        <option value="C">Celcius</option>
        <option value="F">Fahrenheit</option>
        <option value="R">Reamur</option>
    </select>

    <label for="toUnit">Ke satuan:</label>
    <select id="toUnit">
        <option value="C">Celcius</option>
        <option value="F">Fahrenheit</option>
        <option value="R">Reamur</option>
    </select>

    <button onclick="convertTemperature()">Konversi</button>
    <p id="tempResult"></p>

    <h2>2. Operasi Matriks Dasar</h2>
    <button onclick="matrixOperations()">Lakukan Operasi Matriks</button>
    <p id="matrixResult"></p>

    <h2>3. Kalkulator Scientific</h2>
    <label for="scientificInput">Masukkan nilai:</label>
    <input type="number" id="scientificInput" placeholder="Masukkan nilai" />

    <label for="scientificOperation">Pilih operasi:</label>
    <select id="scientificOperation">
        <option value="sqrt">Akar Kuadrat</option>
        <option value="sin">Sinus</option>
        <option value="cos">Kosinus</option>
    </select>

    <button onclick="scientificCalculator()">Hitung</button>
    <p id="scientificResult"></p>

    <script>
        // 1. Konversi Suhu
        function convertTemperature() {
            const inputValue = parseFloat(document.getElementById('tempInput').value);
            const fromUnit = document.getElementById('fromUnit').value;
            const toUnit = document.getElementById('toUnit').value;

            const result = convertTemperatureFormula(inputValue, fromUnit, toUnit);
            document.getElementById('tempResult').innerText = `Hasil konversi suhu: ${result}`;
        }

        function convertTemperatureFormula(value, fromUnit, toUnit) {
            if (fromUnit === 'C' && toUnit === 'F') {
                return (value * 9/5) + 32;
            } else if (fromUnit === 'C' && toUnit === 'R') {
                return value * 4/5;
            } else if (fromUnit === 'F' && toUnit === 'C') {
                return (value - 32) * 5/9;
            } else if (fromUnit === 'F' && toUnit === 'R') {
                return (value - 32) * 4/9;
            } else if (fromUnit === 'R' && toUnit === 'C') {
                return value * 5/4;
            } else if (fromUnit === 'R' && toUnit === 'F') {
                return (value * 9/4) + 32;
            } else {
                return value; // Sama satuan atau kasus tidak valid
            }
        }

        // 2. Operasi Matriks Dasar
        function matrixOperations() {
            const matrixA = tf.tensor2d([[1, 2], [3, 4]]);
            const matrixB = tf.tensor2d([[5, 6], [7, 8]]);

            const transposeMatrixA = matrixA.transpose();
            const sumMatrix = matrixA.add(matrixB);
            const multiplyMatrix = matrixA.matMul(matrixB);
            const inverseMatrixA = matrixA.inverse();

            document.getElementById('matrixResult').innerText = `
                Transpose Matrix A:\n${transposeMatrixA.arraySync()}
                \n\nSum of Matrices A and B:\n${sumMatrix.arraySync()}
                \n\nMultiplication of Matrices A and B:\n${multiplyMatrix.arraySync()}
                \n\nInverse of Matrix A:\n${inverseMatrixA.arraySync()}
            `;
        }

        // 3. Kalkulator Scientific
        function scientificCalculator() {
            const inputValue = parseFloat(document.getElementById('scientificInput').value);
            const operation = document.getElementById('scientificOperation').value;

            const result = scientificCalculatorFormula(operation, inputValue);
            document.getElementById('scientificResult').innerText = `Hasil operasi matematika: ${result}`;
        }

        function scientificCalculatorFormula(operation, value) {
            switch (operation) {
                case 'sqrt':
                    return Math.sqrt(value);
                case 'sin':
                    return Math.sin(value);
                case 'cos':
                    return Math.cos(value);
                default:
                    return NaN; // Operasi tidak valid
            }
        }
    </script>
</body>
</html>